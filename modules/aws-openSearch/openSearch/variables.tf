# variable "elasticsearch_version" {
#   type        = string
#   default     = "7.10"
#   description = "Version of Elasticsearch to deploy"
# }

# variable "zone_awareness_enabled" {
#   type        = bool
#   default     = true
#   description = "Enable zone awareness for Elasticsearch cluster"
# }
# # variable "availability_zones" {
# #   description = "The number of availability zones for the OpenSearch cluster. Valid values: 1, 2 or 3."
# #   type        = number
# #   default     = 3
# # }


####------------------------------------------------------------------------------------

# variable "advanced_security_options" {
#   type        = bool
#   description = "Whether to enable advanced_security_options."
#   default     = true
# }

# variable "internal_user_database_enabled" {
#   type        = bool
#   description = "Whether to enable internal_user_database_enabled."
#   default     = true
# }

# variable "custom_endpoint_enabled" {
#   type        = bool
#   description = "Whether to enable custom endpoint for the Elasticsearch domain."
#   default     = false
# }

# variable "custom_endpoint" {
#   type        = string
#   description = "Fully qualified domain for custom endpoint."
#   default     = "kibana-new.preprod.internal.getvokal.com"
# }

# variable "custom_endpoint_certificate_arn" {
#   type        = string
#   description = "ACM certificate ARN for custom endpoint."
#   default     = "arn:aws:acm:ap-south-1:852303623836:certificate/34cddb18-6e66-47ac-a94a-8aa8b32c73f9"
# }

####------------------------------------------------------------------------------------

variable "create_opensearch" {
  type        = bool
  description = "Set to true to create security group"
  default     = false
}

variable "advanced_options" {
  type        = map(string)
  default     = {}
  description = "Key-value string pairs to specify advanced configuration options"
}

variable "encrypt_at_rest_kms_key_id" {
  type        = string
  default     = ""
  description = "The KMS key ID to encrypt the Elasticsearch domain with. If not specified, then it defaults to using the AWS/Elasticsearch service KMS key"
}

variable "openSerch_security_group_ids" {
  type = list(string)
}

variable "private_subnet" {
  default = []
}

variable "private_subnet_ids" { 
  type = list(string)  
  default = []
} 

 variable "availability_zone_count" {
  description = "Number of availability zones"
  type        = number
 # default     = 2  # Default value, adjust as needed
}

variable "opensearch_cluster_config" {
  type = list(object({
    domain                   = string
    engine_version           = string    
    # advanced_options         = list(string)
    
    instance_count           = number
    instance_type            = string
    zone_awareness_enabled   = bool
    dedicated_master_enabled = bool
    dedicated_master_count   = number
    dedicated_master_type    = string
    warm_instance_enabled    = bool 
    warm_instance_count      = number
    warm_instance_type       = string
    
    ebs_volume_size          = number
    ebs_volume_type          = string
    ebs_iops                 = number

    advanced_security_options       = bool
    internal_user_database_enabled  = bool
    master_user_name                = string
    master_user_password            = string

    custom_endpoint_enabled         = bool 
    custom_endpoint                 = string
    custom_endpoint_certificate_arn = string
 
    encrypt_at_rest_enabled         = bool
    encrypt_at_rest_kms_key_id      = string 
    node_to_node_encryption_enabled = bool
    automated_snapshot_start_hour   = number 
  }))
  default = [ ]
}


