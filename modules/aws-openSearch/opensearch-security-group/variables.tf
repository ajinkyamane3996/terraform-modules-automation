variable "create_opensearch" {
  type        = bool
  description = "Set to true to create security group"
  default     = false
}

variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of ingress rules for the security group"
  #default     = []
}

variable "egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of egress rules for the security group"
  #default     = []
}
variable "opensearch_security_group_tags" {
  type = map(string)
}

 variable "tags" {
   type = map(string)
 }