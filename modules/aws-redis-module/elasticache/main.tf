## Redis Cluster Mode Disabled 

# # Grab the list of availability zones
# data "aws_availability_zones" "available" {}

resource "aws_elasticache_replication_group" "baz" {

  count = var.create_elasticache && length(var.elasticache_cluster_config) > 0 ? length(var.elasticache_cluster_config) : 0
  # count           = "${length(var.private_subnet_ids)}"

  engine                     = var.elasticache_cluster_config[count.index].engine
  engine_version             = var.elasticache_cluster_config[count.index].engine_version 
  
  replication_group_id       = var.elasticache_cluster_config[count.index].replication_group_id
  description                = var.elasticache_cluster_config[count.index].description

  node_type                  = var.elasticache_cluster_config[count.index].node_type
  
  num_cache_clusters         = var.elasticache_cluster_config[count.index].cluster_mode_enabled ?  var.elasticache_cluster_config[count.index].cluster_size : null ##

  port                       = var.elasticache_cluster_config[count.index].port  
  availability_zones         = var.elasticache_cluster_config[count.index].availability_zones
  # availability_zones = [data.aws_availability_zones.available.names[count.index]]

  parameter_group_name       = aws_elasticache_parameter_group.parameter_group[count.index].id ## 
  automatic_failover_enabled = var.elasticache_cluster_config[count.index].automatic_failover_enabled
  multi_az_enabled           = var.elasticache_cluster_config[count.index].multi_az_enabled
  security_group_ids         = var.elastic_security_group_ids
  # security_group_ids         = [aws_security_group.redis_cache_sg.id] ##
  maintenance_window         = var.elasticache_cluster_config[count.index].maintenance_window
  auth_token                 = var.elasticache_cluster_config[count.index].transit_encryption_enabled ? var.elasticache_cluster_config[count.index].auth_token : null
  at_rest_encryption_enabled = var.elasticache_cluster_config[count.index].at_rest_encryption_enabled
  transit_encryption_enabled = var.elasticache_cluster_config[count.index].transit_encryption_enabled || var.elasticache_cluster_config[count.index].auth_token != null
  kms_key_id                 = var.elasticache_cluster_config[count.index].at_rest_encryption_enabled ? var.kms_key_id : null ##
  apply_immediately          = var.elasticache_cluster_config[count.index].apply_immediately
  data_tiering_enabled       = var.elasticache_cluster_config[count.index].data_tiering_enabled
  auto_minor_version_upgrade = var.elasticache_cluster_config[count.index].auto_minor_version_upgrade
  subnet_group_name          = aws_elasticache_subnet_group.example[count.index].name

  # log_delivery_configuration {
  #   destination      = aws_cloudwatch_log_group.cache_cluster_slow_log.name
  #   destination_type = "cloudwatch-logs"
  #   log_format       = "json"
  #   log_type         = "slow-log"
  # } 
  # log_delivery_configuration {
  #   destination      = aws_cloudwatch_log_group.cache_cluster_engine_log.name
  #   destination_type = "cloudwatch-logs"
  #   log_format       = "json"
  #   log_type         = "engine-log"
  # }
  
}


# resource "aws_cloudwatch_log_group" "cache_cluster_slow_log" {
#   name = "prod-cache_cluster_slow_log"
#   tags = {
#     Environment = "production"
#     Application = "service-s"
#   }
# }

# resource "aws_cloudwatch_log_group" "cache_cluster_engine_log" {
#   name = "prod-cache_cluster_engine_log"
#   tags = {
#     Environment = "production"
#     Application = "service-e"
#   }
# }

resource "aws_elasticache_subnet_group" "example" {
  count =  var.create_elasticache && length(var.elasticache_cluster_config) > 0 ?  length(var.elasticache_cluster_config) : 1
  name       = var.elasticache_cluster_config[count.index].elasticache_subnet_group_name
  subnet_ids = var.private_subnet_ids
 #subnet_ids = [ var.private_subnet_ids[0],var.private_subnet_ids[1],var.private_subnet_ids[1]] 
}

resource "aws_elasticache_parameter_group" "parameter_group" {

  count = var.create_elasticache && length(var.elasticache_cluster_config) > 0 ? length(var.elasticache_cluster_config) :  0
  
  name   = var.elasticache_cluster_config[count.index].elasticache_parameter_group_name
  family = var.elasticache_cluster_config[count.index].elasticache_parameter_group_family  

  ## Note : IF need to change parameter values then you can use below on code for it
  # parameter {
  #   name  = "activerehashing"
  #   value = "yes"
  # }  
}


# resource "aws_security_group" "redis_cache_sg" {
#   name   = "redis_cache-uat-sg"
#   vpc_id =  var.vpc_id  #"vpc-0e11aecaffdc70ee7"
#   ingress {
#     description = "redis cache port"
#     from_port   = 6379
#     to_port     = 6379
#     protocol    = "tcp"
#     cidr_blocks = ["10.200.0.0/16"]
#   }
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   tags = {
#     Name               = "uat-redis_cache-sg"
#     Environment        = "uae-shared-uat"
#     BillingOwner       = "SaaS"
#     BillingClient      = "Uae-shared"
#     BillingEnvironment = "UAT"
#   }
# }
