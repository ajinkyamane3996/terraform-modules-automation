variable "create_elasticache" {
  type = bool
}

variable "kms_key_id" {
  type        = string
  description = "The ARN of the key that you wish to use if encrypting at rest. If not supplied, uses service managed encryption. `at_rest_encryption_enabled` must be set to `true`"
  default     = null
}

variable "private_subnet_ids" {
  type    = list(string)
  default = []
}

variable "vpc_id" {
  type = string
}

variable "elastic_security_group_ids" {
  type = list(string)
}

#------- elasticache  ------------------------------------------#

variable "elasticache_cluster_config" {
  type = list(object({
    engine                             = string
    engine_version                     = string   

    replication_group_id               = string
    description                        = string
    node_type                          = string
    cluster_mode_enabled               = bool
    cluster_size                       = number
    port                               = number
    availability_zones                 = list(string)

    automatic_failover_enabled         = bool
    multi_az_enabled                   = bool

    maintenance_window                 = string
    auth_token                         = string
    at_rest_encryption_enabled         = bool
    transit_encryption_enabled         = bool

    apply_immediately                  = bool
    data_tiering_enabled               = bool
    auto_minor_version_upgrade         = bool
    
    elasticache_subnet_group_name      = string
    elasticache_parameter_group_name   = string
    elasticache_parameter_group_family = string
  }))
}

