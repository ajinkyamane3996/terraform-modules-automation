
#Region
variable "region" {
  type = string
}

# Profile
variable "profile" {
  type        = string
  description = "profile name"
}

#ALL DEFAULT TAGS

variable "tags" {
  type = map(string)
}

#VPC
variable "vpc_creation" {
  type = bool
}

variable "vpc_name" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "vpc_enable_dns_hostnames" {
  type = bool
}

variable "vpc_enable_dns_support" {
  type = bool
}

variable "vpc_tags" {
  type = map(string)
}

#central vpc id
variable "vpc_id" {
  type = string
}

#central availablity zone
variable "availability_zone" {
  type = list(string)
}

#private subnets
variable "create_private_subnets" {
  type = bool
}

variable "private_subnets_name" {
  type = string
}

variable "private_subnet_nat" {
  type = string
}

variable "private_subnets" {
  type = list(string)
}

variable "private_subnets_tags" {
  type = map(string)
}

#NAT
variable "nat_create" {
  type = bool
}

variable "nat_name" {
  type = string
}

variable "nat_public_subnet_id" {
  type = string
}

variable "nat_tags" {
  type = map(string)
}

#public subnets
variable "create_public_subnets" {
  type = bool
}

variable "igw_create" {
  type = bool
}

variable "public_subnet_name" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "public_subnets_tags" {
  type = map(string)
}

#database subnets

variable "database_subnet_nat" {
  type = string
}

variable "create_database_subnets" {
  type = bool
}

variable "database_subnets_name" {
  type = string
}

variable "database_subnets" {
  type = list(string)
}

variable "database_subnets_tags" {
  type = map(string)
}

#EKS subnets
variable "create_eks_subnets" {
  type = bool
}

variable "eks_subnets_name" {
  type = string
}

variable "eks_subnet_nat" {
  type = string
}

variable "eks_subnets" {
  type = list(string)
}

variable "eks_subnets_tags" {
  type = map(string)
}

# flow logs cloudwatch

variable "create_flow_logs_cloudwatch" {
  type = bool
}

variable "flow_logs_cloudwatch_name" {
  type = string
}


#flow logs S3
variable "create_flow_logs_s3" {
  type = bool
}

variable "flow_logs_s3_name" {
  type = string
}

## ------------------------ OpenSearch ------------------------------------------------------------

variable "create_opensearch" {
  type = bool
}

variable "openSerch_subnets" {
  type = list(string)
}

variable "use_module_private_subnets" {
  type = bool
}

variable "opensearch_cluster_config" {
  type = list(object({
    domain                   = string
    engine_version           = string
    instance_count           = number
    instance_type            = string
    zone_awareness_enabled   = bool
    dedicated_master_enabled = bool
    dedicated_master_count   = number
    dedicated_master_type    = string
    warm_instance_enabled    = bool
    warm_instance_count      = number
    warm_instance_type       = string

    ebs_volume_size = number
    ebs_volume_type = string
    ebs_iops        = number

    advanced_security_options      = bool
    internal_user_database_enabled = bool
    master_user_name               = string
    master_user_password           = string

    custom_endpoint_enabled         = bool
    custom_endpoint                 = string
    custom_endpoint_certificate_arn = string

    encrypt_at_rest_enabled         = bool
    encrypt_at_rest_kms_key_id      = string
    node_to_node_encryption_enabled = bool
    automated_snapshot_start_hour   = number
  }))
  default = []
}

variable "availability_zone_count" {
  description = "Number of availability zones"
  type        = number
  # default     = 2  # Default value, adjust as needed
}

##------- Opensearch Security group ------------------------------------------####

variable "opensearch_sg_name" {
  type = string
}

variable "openSerch_security_group_ids" {
  type = list(string)
}

variable "opensearch_sg_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of ingress rules for the security group"
  #default     = []
}


variable "opensearch_sg_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of egress rules for the security group"
  #default     = []
}


variable "opensearch_security_group_tags" {
  type = map(string)
}

##------- Elasticache  ------------------------------------------#

variable "create_elasticache" {
  type = bool
}

variable "elasticache_subnets" {
  type = list(string)
}

variable "elastic_security_group_ids" {
  type = list(string)
}

variable "elasticache_cluster_config" {
  type = list(object(
    {
      engine         = string
      engine_version = string

      replication_group_id = string
      description          = string
      node_type            = string
      cluster_mode_enabled = bool
      cluster_size         = number
      port                 = number
      availability_zones   = list(string)

      #parameter_group_name       = 
      automatic_failover_enabled = bool
      multi_az_enabled           = bool
      #security_group_ids         = 
      maintenance_window         = string
      auth_token                 = string
      at_rest_encryption_enabled = bool
      transit_encryption_enabled = bool
      #kms_key_id                 = 
      apply_immediately          = bool
      data_tiering_enabled       = bool
      auto_minor_version_upgrade = bool

      elasticache_subnet_group_name      = string
      elasticache_parameter_group_name   = string
      elasticache_parameter_group_family = string
  }))
}


### ------- Elasticache Security Groups -------------------------------------------------

variable "redis_sg_name" {
  type = string
}

variable "redis_sg_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of ingress rules for the security group"
  #default     = []
}


variable "redis_sg_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of egress rules for the security group"
  #default     = []
}

variable "redis_security_group_tags" {
  type = map(string)
}