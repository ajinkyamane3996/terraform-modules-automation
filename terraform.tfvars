#region
# region = "ap-south-1"
region  = "us-east-1"
profile = "ajinkya-tf"

#default tags
tags = {
  "owner" = "Ajinkya"
}

#central availbilty zones
availability_zone = ["us-east-1a", "us-east-1b", "us-east-1c"]
#availability_zone = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]

#If you are creating VPC then the value here must be blank string "". if not creating `VPC` use this to pass vpc_id to all resources. values passed here will be used as vpc id.
vpc_id = "" #"vpc-123456789"

##--------VPC ---------------------------------------------------------------------------------------
vpc_creation             = true
vpc_name                 = "changeme"
vpc_cidr_block           = "10.200.0.0/16"
vpc_enable_dns_hostnames = true
vpc_enable_dns_support   = true
vpc_tags = {
  "Managed by" = "Terraform"
}


##------- public subnets ---------------------------------------------------------------------------------------
create_public_subnets = true
igw_create            = true
public_subnet_name    = "Production"
public_subnets        = ["10.200.101.0/24", "10.200.102.0/24", "10.200.103.0/24"]
public_subnets_tags = {
  "Managed by" = "Terraform"
}


##------- private subnets ---------------------------------------------------------------------------------------
create_private_subnets = true
private_subnets_name   = "production"
private_subnet_nat     = ""                                                  # ID of the NAT gateway in case you are not creating NAT from here. If creating leave it blank.
private_subnets        = ["10.200.1.0/24", "10.200.2.0/24", "10.200.3.0/24"] #if you are not creating private subnets then empty the list to reduce conflicts "[]" and set create_private_subnets = false.
private_subnets_tags = {
  "Managed by" = "Terraform"
}

##------- NAT ---------------------------------------------------------------------------------------

nat_create           = true
nat_name             = "production"
nat_public_subnet_id = "" # Nat-1234567890" Optional. If you are not creating public subnets here then specify Public subnet id to create public nat gateway. If not creating NAT (false) then specify random value. If creating then leave it blank.
nat_tags = {
  "Managed by" = "Terraform"
}



##------- database subnets ---------------------------------------------------------------------------------------
create_database_subnets = true
database_subnets_name   = "production"
database_subnet_nat     = ""                                                  # ID of the NAT gateway in case you are not creating NAT from here. If creating leave it blank.
database_subnets        = ["10.200.4.0/24", "10.200.5.0/24", "10.200.6.0/24"] #if you are not creating database subnets then empty the list to reduce conflicts "[]" and set create_database_subnets = false.
database_subnets_tags = {
  "Managed by" = "Terraform"
}

##------- eks subnets ---------------------------------------------------------------------------------------
create_eks_subnets = true
eks_subnets_name   = "production"
eks_subnet_nat     = ""                                                  # ID of the NAT gateway in case you are not creating NAT from here. If creating leave it blank.
eks_subnets        = ["10.200.7.0/24", "10.200.8.0/24", "10.200.9.0/24"] #if you are not creating eks subnets then empty the list to reduce conflicts "[]"  and set create_eks_subnets = false.
eks_subnets_tags = {
  "Managed by" = "Terraform"
}

##------- flow logs cloudwatch ---------------------------------------------------------------------------------------

create_flow_logs_cloudwatch = false
flow_logs_cloudwatch_name   = "production"

##------- flow logs s3 ---------------------------------------------------------------------------------------

create_flow_logs_s3 = false
flow_logs_s3_name   = "production"


## ------ OpenSearch ---------------------------------------------------------------------------------------

create_opensearch            = true
use_module_private_subnets   = true
openSerch_subnets            = []
openSerch_security_group_ids = []
opensearch_cluster_config    = [
  {
    domain                 = "newdomain"
    engine_version         = "OpenSearch_2.3" #"7.10"
    instance_count         = 3
    instance_type          = "t3.medium.search"
    zone_awareness_enabled = true

    dedicated_master_enabled = true
    dedicated_master_count   = 3
    dedicated_master_type    = "t3.medium.search"

    warm_instance_enabled = false
    warm_instance_count   = 2
    warm_instance_type    = "ultrawarm1.medium.search"

    ebs_volume_size = 10
    ebs_volume_type = "gp3"
    ebs_iops        = 3000

    advanced_security_options      = true
    internal_user_database_enabled = true
    master_user_name               = "esadmin"
    master_user_password           = "ESadmin@5678910"

    custom_endpoint_enabled         = false
    custom_endpoint                 = "" ## optinal
    custom_endpoint_certificate_arn = "" ## optinal

    encrypt_at_rest_enabled    = true
    encrypt_at_rest_kms_key_id = "" ## optinal

    node_to_node_encryption_enabled = true
    automated_snapshot_start_hour   = 23
  }
]

availability_zone_count = 3

##-------------- Opensearch security group --------------------------------------------------------------

opensearch_sg_name = "opensearch-dr-sg-hyderbad"

opensearch_sg_ingress_rules = [{
  cidr_blocks = ["10.70.0.0/16"]
  description = "Allowed from vpc cidr"
  from_port   = 443
  protocol    = "tcp"
  to_port     = 443
}]

opensearch_sg_egress_rules = [{
  cidr_blocks = ["0.0.0.0/0"]
  description = "Allowed from vpc cidr"
  from_port   = 0
  protocol    = "-1"
  to_port     = 0
}]

opensearch_security_group_tags = {
  "Name" = "opensearch-sg"
}

## ------ Elasticache -----------------------------------------------------------

create_elasticache  = false
elasticache_subnets = []
elastic_security_group_ids = []
elasticache_cluster_config = [
  {
    engine                             = "redis"
    engine_version                     = "7.0"
    replication_group_id               = "tf-redis-cluster-00"
    description                        = "test description"
    node_type                          = "cache.t2.small"
    cluster_mode_enabled               = true
    cluster_size                       = 2 ## value of this parameter must be at least 2.
    port                               = 6369
    availability_zones                 = ["us-east-1a", "us-east-1b"]
    automatic_failover_enabled         = true
    multi_az_enabled                   = true
    maintenance_window                 = "wed:03:00-wed:04:00"
    auth_token                         = "2ED965LIK4kqZZlzHgSO"
    at_rest_encryption_enabled         = true
    transit_encryption_enabled         = true
    apply_immediately                  = true
    data_tiering_enabled               = false
    auto_minor_version_upgrade         = false
    elasticache_subnet_group_name      = "Changeme-pod-cache-subnet-00"
    elasticache_parameter_group_name   = "cache-params-00"
    elasticache_parameter_group_family = "redis7"
  } #,
  # {
  #   engine         = "redis"
  #   engine_version = "7.0"

  #   replication_group_id       = "tf-redis-cluster-01"
  #   description                = "test description"
  #   node_type                  = "cache.t2.small"
  #   cluster_mode_enabled       = true
  #   cluster_size               = 3 ## value of this parameter must be at least 2.
  #   port                       = 6369
  #   availability_zones         = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
  #   automatic_failover_enabled = true
  #   multi_az_enabled           = true
  #   maintenance_window         = "wed:03:00-wed:04:00"
  #   auth_token                 = "2ED965LIK4kqZZlzHgSO"
  #   at_rest_encryption_enabled = true
  #   transit_encryption_enabled = true
  #   apply_immediately          = true
  #   data_tiering_enabled       = false
  #   auto_minor_version_upgrade = false

  #   elasticache_subnet_group_name      = "Changeme-pod-cache-subnet-01"
  #   elasticache_parameter_group_name   = "cache-params-01"
  #   elasticache_parameter_group_family = "redis7"
  # }
]

##------ Elasticache Security Groups ------------------------------------------------------------

redis_sg_name      = "Redis-DR-SG-hyderbad"
redis_sg_ingress_rules = [{
  cidr_blocks = ["10.70.0.0/16"] #["10.150.0.0/16"] ##["10.0.4.0/24"] 
  description = "this is subnet for db"
  from_port   = 6379
  protocol    = "tcp"
  to_port     = 6379
}]

redis_sg_egress_rules = [{
  cidr_blocks = ["0.0.0.0/0"]
  description = "internet connectivity"
  from_port   = 0
  protocol    = "-1"
  to_port     = 0
}]

redis_security_group_tags = {
  "Name" = "redis-sg"
}


