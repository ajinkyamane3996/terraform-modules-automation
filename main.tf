module "vpc" {
  source               = "./modules/vpc-components/vpc"
  vpc_creation         = var.vpc_creation
  name                 = var.vpc_name
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = var.vpc_enable_dns_support
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  vpc_tags             = var.vpc_tags
  tags = merge(
    var.tags
  )
}

module "nat" {
  source           = "./modules/vpc-components/NAT"
  nat_create       = var.nat_create && var.create_private_subnets && length(var.private_subnets) > 0 ? true : false
  name             = var.nat_name
  public_subnet_id = var.nat_public_subnet_id != "" ? var.nat_public_subnet_id : module.public_subnets.public_subnets[0]
  nat_tags         = var.nat_tags
  tags = merge(
    var.tags
  )
}

module "private_subnets" {
  source                 = "./modules/vpc-components/private_subnets"
  name                   = var.private_subnets_name
  create_private_subnets = var.create_private_subnets
  private_subnets        = var.private_subnets
  vpc_id                 = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone      = var.availability_zone
  private_subnet_nat     = var.private_subnet_nat != "" ? var.private_subnet_nat : join(",", module.nat.nat_id)
  tags                   = var.tags
  private_subnets_tags   = var.private_subnets_tags
}

module "public_subnets" {
  source                = "./modules/vpc-components/public_subnets"
  create_public_subnets = var.create_public_subnets
  igw_create            = var.igw_create
  name                  = var.public_subnet_name
  public_subnets        = var.public_subnets
  vpc_id                = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone     = var.availability_zone
  tags                  = var.tags
  public_subnets_tags   = var.public_subnets_tags
}

module "database_subnets" {
  source                  = "./modules/vpc-components/database_subnets"
  name                    = var.database_subnets_name
  create_database_subnets = var.create_database_subnets
  database_subnets        = var.database_subnets
  vpc_id                  = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone       = var.availability_zone
  database_subnet_nat     = var.database_subnet_nat != "" ? var.database_subnet_nat : join(",", module.nat.nat_id)
  tags                    = var.tags
  database_subnets_tags   = var.database_subnets_tags
}

module "eks_subnets" {
  source             = "./modules/vpc-components/eks_subnets"
  name               = var.eks_subnets_name
  create_eks_subnets = var.create_eks_subnets
  eks_subnets        = var.eks_subnets
  vpc_id             = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone  = var.availability_zone
  eks_subnet_nat     = var.eks_subnet_nat != "" ? var.eks_subnet_nat : join(",", module.nat.nat_id)
  tags               = var.tags
  eks_subnets_tags   = var.eks_subnets_tags
}

module "flow_logs_cloudwatch" {
  source                      = "./modules/vpc-components/flow_logs_cloudwatch"
  create_flow_logs_cloudwatch = var.create_flow_logs_cloudwatch
  vpc_id                      = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  name                        = var.flow_logs_cloudwatch_name
}

module "flow_logs_s3" {
  source              = "./modules/vpc-components/flow_logs_S3"
  create_flow_logs_s3 = var.create_flow_logs_s3
  vpc_id              = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  name                = var.flow_logs_s3_name
}

## ------------------------ OpenSearch ------------------------------------------------------------

module "openSerch" {
  source = "./modules/aws-openSearch/openSearch"

  create_opensearch            = var.create_opensearch
  private_subnet               = var.use_module_private_subnets ?  var.private_subnets : var.openSerch_subnets
  private_subnet_ids           = var.use_module_private_subnets ?  module.private_subnets.private_subnets[*] : var.openSerch_subnets
  openSerch_security_group_ids = length(var.openSerch_security_group_ids)> 0 ? var.openSerch_security_group_ids :  module.opensearch_security_group.opensearch_sg_ids
  availability_zone_count      = var.availability_zone_count
  opensearch_cluster_config    = var.opensearch_cluster_config
}

module "opensearch_security_group" {
  source                         = "./modules/aws-openSearch/opensearch-security-group"

  create_opensearch              = var.create_opensearch
  name                           = var.opensearch_sg_name
  vpc_id                         = var.use_module_private_subnets  ? module.vpc.vpc_id[0] : var.vpc_id
  ingress_rules                  = var.opensearch_sg_ingress_rules
  egress_rules                   = var.opensearch_sg_egress_rules
  opensearch_security_group_tags = var.opensearch_security_group_tags
  tags = merge(
    var.tags
  )
}

## ------------------------ ElastiCache  ------------------------------------------------

module "elasticache" {
  source = "./modules/aws-redis-module/elasticache"

  create_elasticache         = var.create_elasticache
  private_subnet_ids         = var.use_module_private_subnets ? module.private_subnets.private_subnets[*] : var.elasticache_subnets
  vpc_id                     = var.use_module_private_subnets  ? module.vpc.vpc_id[0] : var.vpc_id
  elastic_security_group_ids = length(var.elastic_security_group_ids) > 0 ? var.elastic_security_group_ids : module.redis_security_group.redis_sg_ids[*]
  elasticache_cluster_config = var.elasticache_cluster_config
}

module "redis_security_group" {
  source                    = "./modules/aws-redis-module/redis-cluster-sg"

  create_elasticache        = var.create_elasticache
  name                      = var.redis_sg_name
  vpc_id                    = var.use_module_private_subnets ?  module.vpc.vpc_id[0] : var.vpc_id
  ingress_rules             = var.redis_sg_ingress_rules
  egress_rules              = var.redis_sg_egress_rules
  redis_security_group_tags = var.redis_security_group_tags
  tags = merge(
    var.tags
  )
}

#---------------------- KMS --------------------------------------

module "kms" {
  
}

## ---------------------- EC2 -----------------------------------

module "ec2" {
  
}

